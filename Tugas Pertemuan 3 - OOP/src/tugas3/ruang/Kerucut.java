/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas3.ruang;

import tugas3.bidang.Lingkaran;

/**
 *
 * @author Orenji
 */
public class Kerucut extends Lingkaran implements MenghitungRuang {
    private double tinggi;

    public Kerucut(double jariJari, double tinggi) {
        super(jariJari);
        this.tinggi = tinggi;
    }

    public Kerucut() {
        super();
        tinggi = 0;
    }
    
    private double sisiMiring(){
        return Math.sqrt(Math.pow(super.getJariJari(), 2) + Math.pow(tinggi, 2));
    }
    
    @Override
    public double volume(){
        return super.luas()*tinggi/3;
    }

    @Override
    public double luasPermukaan(){
        return super.luas() + Math.PI*super.getJariJari()*sisiMiring();
    }

    public double getTinggi() {
        return tinggi;
    }

    public void setTinggi(double tinggi) {
        this.tinggi = tinggi;
    }
}
