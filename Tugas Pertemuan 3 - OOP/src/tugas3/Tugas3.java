/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas3;

import java.util.Scanner;
import tugas3.ruang.Balok;
import tugas3.ruang.Kerucut;

/**
 *
 * @author Orenji
 */
public class Tugas3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        double jariJari, panjang, lebar, tinggi;
        
        while(true){
            System.out.println("INPUT ");
            System.out.println("1. BALOK");
            System.out.println("2. KERUCUT");
            System.out.println("3. EXIT");
            System.out.print("PILIH : ");
            char pilih = scanner.next().charAt(0);

            Balok balok;
            Kerucut kerucut;
            
            switch (pilih){
                case '1':
                    System.out.print("PANJANG = ");
                    panjang = scanner.nextDouble();
                    System.out.print("LEBAR   = ");
                    lebar = scanner.nextDouble();
                    System.out.print("TINGGI  = ");
                    tinggi = scanner.nextDouble();
                    
                    balok = new Balok(panjang, lebar, tinggi);
                    
                    System.out.println("________ OUTPUT ________");
                    System.out.println("LUAS PERSEGI         : " + balok.luas());
                    System.out.println("KELILING PERSEGI     : " + balok.keliling());
                    System.out.println("VOLUME BALOK         : " + balok.volume());
                    System.out.println("LUAS PERMUKAAN BALOK : " + balok.luasPermukaan());
                    break;
                case '2':
                    System.out.print("JARI-JARI = ");
                    jariJari = scanner.nextDouble();
                    System.out.print("TINGGI    = ");
                    tinggi = scanner.nextDouble();
                    
                    kerucut = new Kerucut(jariJari, tinggi);
                    
                    System.out.println("________ OUTPUT ________");
                    System.out.println("LUAS LINGKARAN         : " + kerucut.luas());
                    System.out.println("KELILING LINGKARAN     : " + kerucut.keliling());
                    System.out.println("VOLUME KERUCUT         : " + kerucut.volume());
                    System.out.println("LUAS PERMUKAAN KERUCUT : " + kerucut.luasPermukaan());
                    break;
                case '3':
                    System.exit(0);
                    break;
                default :
                    System.out.println("INPUT SALAH!!!");
            }
        }
    }
}
