/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamahasiswa;

import java.util.Scanner;

/**
 *
 * @author RTA
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        String name, nim, address;
        int uts, uas;
        
        System.out.print("Masukkan nama : ");
        name = scanner.nextLine();
        
        System.out.print("Masukkan nim : ");
        nim = scanner.next();
        
        scanner = new Scanner(System.in);
        System.out.print("Masukkan alamat : ");
        address = scanner.nextLine();
        
        System.out.print("Masukkan nilai uts : ");
        uts = scanner.nextInt();
        
        System.out.print("Masukkan nilai uas : ");
        uas = scanner.nextInt();
        
        Mahasiswa mahasiswa = new Mahasiswa(name, nim, address, uts, uas);
        
        char loop;
        do {
            char menu;
            
            System.out.println("Menu");
            System.out.println("1. Lihat Detail Data");
            System.out.println("2. Edit Data");
            System.out.print("Pilih : ");
            menu = scanner.next().charAt(0);
            
            switch (menu){
                case '1': 
                    mahasiswa.showDetail();
                    break;
                case '2':
                    scanner = new Scanner(System.in);
                    System.out.print("Masukkan nama : ");
                    mahasiswa.name = scanner.nextLine();
                    
                    System.out.print("Masukkan nim : ");
                    mahasiswa.nim = scanner.next();
                    
                    scanner = new Scanner(System.in);
                    System.out.print("Masukkan alamat : ");
                    mahasiswa.address = scanner.nextLine();
                    
                    System.out.print("Masukkan nilai uts : ");
                    mahasiswa.uts = scanner.nextInt();
                    
                    System.out.print("Masukkan nilai uas : ");
                    mahasiswa.uas = scanner.nextInt();
                    break;
            }
            
            System.out.print("Back to menu (y/n) :");
            loop = scanner.next().charAt(0);
            loop = Character.toLowerCase(loop);
        } while (loop == 'y');
    }
}