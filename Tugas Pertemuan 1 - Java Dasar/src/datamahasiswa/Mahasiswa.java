/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamahasiswa;

/**
 *
 * @author RTA
 */
public class Mahasiswa {
    String name;
    String nim;
    String address;
    int uts;
    int uas;

    public Mahasiswa(String name, String nim, String address, int uts, int uas) {
        this.name = name;
        this.nim = nim;
        this.address = address;
        this.uts = uts;
        this.uas = uas;
    }

    String jurusan(){
        String jurusan;
        jurusan = "";
        switch (nim.charAt(2)){
            case '1': 
                jurusan = "Teknik Kimia";
                break;
            case '2':
                jurusan = "Teknik Industri";
                break;
            case '3':
                jurusan = "Informatika";
                break;
            case '4':
                jurusan = "Sistem Informasi";
                break;
            default :
                jurusan = "angkatan not found!";
        }
        return jurusan;
    }
    
    double nilaiAkhir(){
        return (uts+uas) / 2;
    }
    
    void showDetail(){
        System.out.println("Nama    : " + name);
        System.out.println("Nim     : " + nim);
        System.out.println("Alamat  : " + address);
        System.out.println("Jurusan : " + jurusan());
        System.out.println("Nilai   : " + nilaiAkhir());
    }
}
