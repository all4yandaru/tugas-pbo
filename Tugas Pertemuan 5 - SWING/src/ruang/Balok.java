/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ruang;

import bidang.Persegi;

/**
 *
 * @author RTA
 */
public class Balok extends Persegi implements MenghitungRuang{
    private double tinggi;

    public Balok(double panjang, double lebar, double tinggi) {
        super(panjang, lebar);
        this.tinggi = tinggi;
    }

    @Override
    public double volume(){
        return super.luas()*tinggi;
    }

    @Override
    public double luasPermukaan(){
        return 2*super.luas() + 2*super.lebar*tinggi + 2*super.panjang*tinggi;
    }  
}
