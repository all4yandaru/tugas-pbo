package Main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import ruang.Balok;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author RTA
 */
public class Main {
    public static void main(String[] args) {
        View kalkulator = new View();
                
        kalkulator.bHitung.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Double panjang, lebar, tinggi;
                Balok balok;
                
                try {
                    panjang = Double.parseDouble(kalkulator.getPanjang());
                    lebar = Double.parseDouble(kalkulator.getLebar());
                    tinggi = Double.parseDouble(kalkulator.getTinggi());
                    
                    balok = new Balok(panjang, lebar, tinggi);
                    
                    String hasil;
                    
                    hasil = "Luas Persegi         : " + balok.luas();
                    kalkulator.lLuasPersegi.setText(hasil);
                    
                    hasil = "Keliling Persegi     : " + balok.keliling();
                    kalkulator.lKelilingPersegi.setText(hasil);
                    
                    hasil = "Volume Balok         : " + balok.volume();
                    kalkulator.lVolumeBalok.setText(hasil);
                    
                    hasil = "Luas Permukaan Balok : " + balok.luasPermukaan();
                    kalkulator.lLuasPermukaanBalok.setText(hasil);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());   
                }
            }
        });
        
        kalkulator.bReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                kalkulator.tfPanjang.setText("");
                kalkulator.tfLebar.setText("");
                kalkulator.tfTinggi.setText("");
                kalkulator.lLuasPersegi.setText("");
                kalkulator.lKelilingPersegi.setText("");
                kalkulator.lVolumeBalok.setText("");
                kalkulator.lLuasPermukaanBalok.setText("");
            }
        });
    }
}
