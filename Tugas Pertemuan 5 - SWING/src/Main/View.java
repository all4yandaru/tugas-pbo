package Main;

import java.awt.HeadlessException;
import javax.swing.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author RTA
 */
public class View extends JFrame{
    JLabel lTitle = new JLabel("KALKULATOR BALOK");
    JLabel lPanjang = new JLabel("Panjang");
    JLabel lLebar = new JLabel("Lebar");
    JLabel lTinggi = new JLabel("Tinggi");
    JLabel lHasil = new JLabel("Hasil : ");
    JLabel lLuasPersegi = new JLabel();
    JLabel lKelilingPersegi = new JLabel();
    JLabel lVolumeBalok = new JLabel();
    JLabel lLuasPermukaanBalok = new JLabel();

    JTextField tfPanjang = new JTextField();
    JTextField tfLebar = new JTextField();
    JTextField tfTinggi = new JTextField();
    
    JButton bHitung = new JButton("Hitung");
    JButton bReset = new JButton("Reset");

    public View(){
        setLayout(null);
        setSize(450,450);
        setVisible(true);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        add(lTitle);
        add(lLebar);
        add(lPanjang);
        add(lTinggi);
        add(lHasil);
        add(lLuasPersegi);
        add(lKelilingPersegi);
        add(lVolumeBalok);
        add(lLuasPermukaanBalok);
        add(tfLebar);
        add(tfPanjang);
        add(tfTinggi);
        add(bHitung);
        add(bReset);
        
        lTitle.setBounds(150, 20, 200, 20);
        
        lPanjang.setBounds(50, 100, 100, 20);
        lLebar.setBounds(50, 150, 100, 20);
        lTinggi.setBounds(50, 200, 100, 20);
        
        tfPanjang.setBounds(150, 100, 200, 20);
        tfLebar.setBounds(150, 150, 200, 20);
        tfTinggi.setBounds(150, 200, 200, 20);
        
        lHasil.setBounds(155, 230, 100, 20);
        
        lLuasPersegi.setBounds(50, 260, 200, 20);
        lKelilingPersegi.setBounds(50, 290, 200, 20);
        lVolumeBalok.setBounds(50, 320, 200, 20);
        lLuasPermukaanBalok.setBounds(50, 350, 200, 20);
        
        bHitung.setBounds(135, 380, 80, 20);
        bReset.setBounds(225, 380, 80, 20);
    }
    
    public String getPanjang(){
        return tfPanjang.getText();
    }
    
    public String getLebar(){
        return tfLebar.getText();
    }
    
    public String getTinggi(){
        return tfTinggi.getText();
    }
}
